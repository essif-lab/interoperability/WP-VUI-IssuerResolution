# Test Suite

This Test Suite tests the implementation of the services provided by the different implementors.




## Getting Started

> TBD - Instructions, how to setup and launch the test suite.

## Test Status

| Case  | Implementor 1 Status | Implementor 2 Status | ... Implementor N Status |All Implementors|
|---|---|---|---|---|
|  Case 1 | [ ]  | [ ] | [ ] | [ ] |

## Test Cases for WP-NAME

### Case 1

#### Case 1 succeeds

- should return ``200`` on assumption X
```json
{
  "field": "value"
}
```

- should return ``201`` on assumption Y

#### Case 1 fails

- should return `400` when error A
- should return `400` when error B
- should return `403` when error C
- should return `404` when error D
