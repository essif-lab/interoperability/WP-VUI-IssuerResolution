# Working Package Summary

Working Package: 2

Working Package Name: Issuer Resolution API

Status: Open Call

Start date:

## Participants

> A Working Package must have at least two implementors and one integrator

| Role | Company | Contact Name | Contact Email
|---|---|---|---|
| Coordinator | GATACA | Irene Hernandez| irene@gataca.io
| Implementor| GATACA| Jose San Juan| jose@gataca.io
| Implementor |  UBICUA |  Andrei Mikhin |  andrei@ubicua.com |
| Integrator |  Letstrust | Dominik Beron | dominik@letstrust.id |
| Observator |  WordpreSSI |  Massimo Romano |  massimo.romano@associazioneblockchain.it|


# Terminology

* **Decentralized Identifier (DID)** as defined in [DID-CORE](https://gataca-io.github.io/verifier-apis/#bib-did-core)
  
* **DID document** as defined in [DID-CORE](https://gataca-io.github.io/verifier-apis/#bib-did-core)

* **DID resolution** as defined in [DID-RESOLUTION](https://gataca-io.github.io/verifier-apis/#bib-did-resolution)
  
* **Revocation List 2020** as defined in [REVOCATION-LIST-2020](https://w3c-ccg.github.io/vc-status-rl-2020/)   
  
* **Verifier** as defined in [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)

* **Verifiable Credential (VC)** as defined in
* [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)

* **Verifiable Presentation (VP)** as defined in [VC-DATA-MODEL](https://gataca-io.github.io/verifier-apis/#bib-vc-data-model)


## Scope

* [Issuer registry data model]()
* [Levels of assurance]()
* [Trust chain personalization]()
* [Issuer common representation]()

## Standard drafts and interoperability specs to be consulted 

* Related to EssifLab IOC project TRAIN by Fraunhofer SIT. Relation to be defined.
* EBSI Trusted Regristries (not publicly available yet?)

## Participate:
* **Apply as Implementor to this WP**: Create a Merge Request on GitLab editing the ReadMe of the corresponding WP. Set the existing WP Coordinator as reviewer.

* **Apply as Integrator to this WP**: Create a Merge Request on GitLab editing the ReadMe of the corresponding WP. Set the existing WP Coordinator as reviewer.

* **File a bug or comments to the WP**:
  - Create new Issues
  - Comment on existing Issues
  - Suggest improvement by opening a Merge Requests of the repository (publicly available for reading, need a Gitlab account for writing)
  - Or else [Mail To: vui@groups.io](mailto://vui@groups.io)

* **Request Access to GitLab**, or any other issue or process if no existing access to GitLab: [Mail To: vui@groups.io](mailto://vui@groups.io)







